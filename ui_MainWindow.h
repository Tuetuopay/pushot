/********************************************************************************
** Form generated from reading UI file 'MainWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionGeneral;
    QAction *actionCredentials;
    QAction *actionUpdates;
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QToolBar *toolBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(400, 284);
        actionGeneral = new QAction(MainWindow);
        actionGeneral->setObjectName(QStringLiteral("actionGeneral"));
        actionGeneral->setCheckable(true);
        actionGeneral->setChecked(true);
        QIcon icon;
        icon.addFile(QStringLiteral(":/tabs/resources/General.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionGeneral->setIcon(icon);
        actionCredentials = new QAction(MainWindow);
        actionCredentials->setObjectName(QStringLiteral("actionCredentials"));
        actionCredentials->setCheckable(true);
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/tabs/resources/Credentials.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionCredentials->setIcon(icon1);
        actionUpdates = new QAction(MainWindow);
        actionUpdates->setObjectName(QStringLiteral("actionUpdates"));
        actionUpdates->setCheckable(true);
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/tabs/resources/Updates.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionUpdates->setIcon(icon2);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        MainWindow->setCentralWidget(centralwidget);
        toolBar = new QToolBar(MainWindow);
        toolBar->setObjectName(QStringLiteral("toolBar"));
        toolBar->setMovable(false);
        toolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        toolBar->setFloatable(false);
        MainWindow->addToolBar(Qt::TopToolBarArea, toolBar);

        toolBar->addAction(actionGeneral);
        toolBar->addAction(actionCredentials);
        toolBar->addAction(actionUpdates);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "PuShot", 0));
        actionGeneral->setText(QApplication::translate("MainWindow", "General", 0));
#ifndef QT_NO_TOOLTIP
        actionGeneral->setToolTip(QApplication::translate("MainWindow", "General Settings", 0));
#endif // QT_NO_TOOLTIP
        actionCredentials->setText(QApplication::translate("MainWindow", "Credentials", 0));
        actionUpdates->setText(QApplication::translate("MainWindow", "Updates", 0));
        toolBar->setWindowTitle(QApplication::translate("MainWindow", "toolBar", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
