/********************************************************************************
 *																				*
 *		PS_Utils.h - Created 25/08/2014 11:10									*
 *																				*
 *		By Tuetuopay - http://tuetuopay.fr										*
 *																				*
 *		This file is part of PuShot.											*
 *																				*
 *		PuShot is free software: you can redistribute it and/or modify			*
 *		it under the terms of the GNU General Public License as published by	*
 *		the Free Software Foundation, either version 3 of the License, or		*
 *		(at your option) any later version.										*
 *																				*
 *		PuShot is distributed in the hope that it will be useful,				*
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of			*
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
 *		GNU General Public License for more details.							*
 *																				*
 *		You should have received a copy of the GNU General Public License		*
 *		along with PuShot.  If not, see <http://www.gnu.org/licenses/>.			*
 *																				*
 ********************************************************************************/

#ifndef _PS_UTILS_H
#define _PS_UTILS_H
#include <QtWidgets>
#include <QtGui>

namespace PS_Utils {
	QString getDesktopLocation ();
	QString getPicturesLocation ();
	
	QStringList listDiff (const QStringList &list1, const QStringList &list2);
	
	void delay (int msec);
	
	QByteArray pixmap2byteArray (const QPixmap &pixmap);
	
	QString getTime ();
	bool savePixmap (const QPixmap &pixmap, const QString &filename);
	bool saveData (const QByteArray &data, const QString &filename);
	QRect getDesktopGeometry ();
	QRect getActiveWindowGeometry ();
	
	QRect getRect (QPoint p1, QPoint p2);
}

#endif /* defined (_PS_UTILS_H) */
