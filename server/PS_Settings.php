<?php

/********************************************************************************
 *																				*
 *		PS_Settings.php - Created 04/09/2014 22:03								*
 *																				*
 *		By Tuetuopay - http://tuetuopay.fr										*
 *																				*
 *		This file is part of PuShot.											*
 *																				*
 *		PuShot is free software: you can redistribute it and/or modify			*
 *		it under the terms of the GNU General Public License as published by	*
 *		the Free Software Foundation, either version 3 of the License, or		*
 *		(at your option) any later version.										*
 *																				*
 *		PuShot is distributed in the hope that it will be useful,				*
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of			*
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
 *		GNU General Public License for more details.							*
 *																				*
 *		You should have received a copy of the GNU General Public License		*
 *		along with PuShot.  If not, see <http://www.gnu.org/licenses/>.			*
 *																				*
 ********************************************************************************/

/* === SQL Database Config === */
$sqlHost = 'localhost';
$sqlDB = 'database';
$sqlUser = 'username';
$sqlPass = 'password';
$sqlMainTable = 'pushot_up';

/* === General Settings === */
$logErrorsToFile = true;
$logFile = 'pushot.log';

/* === Credentials ==== */
$needsLogin = true;
$sqlUsersTable = 'pushot_users';

/* === File storage === */
$storageFolder = 'files/';
$baseURL = 'i.tuetuopay.fr';

?>