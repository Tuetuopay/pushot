/********************************************************************************
 *																				*
 *		PS_TabGeneral.h - Created 25/08/2014 01:23								*
 *																				*
 *		By Tuetuopay - http://tuetuopay.fr										*
 *																				*
 *		This file is part of PuShot.											*
 *																				*
 *		PuShot is free software: you can redistribute it and/or modify			*
 *		it under the terms of the GNU General Public License as published by	*
 *		the Free Software Foundation, either version 3 of the License, or		*
 *		(at your option) any later version.										*
 *																				*
 *		PuShot is distributed in the hope that it will be useful,				*
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of			*
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
 *		GNU General Public License for more details.							*
 *																				*
 *		You should have received a copy of the GNU General Public License		*
 *		along with PuShot.  If not, see <http://www.gnu.org/licenses/>.			*
 *																				*
 ********************************************************************************/

#ifndef _PS_TAB_GENERAL_H
#define _PS_TAB_GENERAL_H

#include <QtGui>

#include "ui_TabGeneral.h"

#include "PS_Tab.h"

class PS_TabGeneral : public PS_Tab, protected Ui_TabGeneral	{
	Q_OBJECT
public:
	PS_TabGeneral (QWidget *parent = NULL);
	~PS_TabGeneral ();
	
	QString screenshotsPath () const { return leScreenshotPath->text (); }
	bool notifSound () const;
	bool notifVisual () const;
	bool openInBrowser () const;
	bool deleteFile () const;
	
	void saveSettings (QSettings &s);
	void loadSettings (QSettings &s);
	
public slots:
	void on_btnBrowse_clicked ();
	void on_leScreenshotPath_textChanged (QString text);
	
signals:
	void screenshotsPathChanged (const QString &path);
};

#endif /* defined (_PS_TAB_GENERAL_H) */
