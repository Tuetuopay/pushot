/********************************************************************************
 *																				*
 *		PS_TabCredentials.cpp - Created 25/08/2014 01:19						*
 *																				*
 *		By Tuetuopay - http://tuetuopay.fr										*
 *																				*
 *		This file is part of PuShot.											*
 *																				*
 *		PuShot is free software: you can redistribute it and/or modify			*
 *		it under the terms of the GNU General Public License as published by	*
 *		the Free Software Foundation, either version 3 of the License, or		*
 *		(at your option) any later version.										*
 *																				*
 *		PuShot is distributed in the hope that it will be useful,				*
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of			*
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
 *		GNU General Public License for more details.							*
 *																				*
 *		You should have received a copy of the GNU General Public License		*
 *		along with PuShot.  If not, see <http://www.gnu.org/licenses/>.			*
 *																				*
 ********************************************************************************/

#include "PS_TabCredentials.h"

PS_TabCredentials::PS_TabCredentials (QWidget *parent) : PS_Tab (parent)	{
	setupUi (this);
	saveSize ();
}

void PS_TabCredentials::saveSettings (QSettings &s)	{
	s.beginGroup ("pushot");
	s.setValue ("login", "account_login");
	s.setValue ("password", "account_password");
	s.endGroup ();
	
	s.beginGroup ("custom");
	s.setValue ("needsLogin", cbNeedsLogin->isChecked ());
	s.setValue ("login", leLogin->text ());
	s.setValue ("password", lePassword->text ());
	s.setValue ("useSsl", cbSsl->isChecked ());
	s.endGroup ();
	
	s.setValue ("url", leApiUrl->text ());
	s.setValue ("mode", (rbPuShotServer->isChecked ()) ? 1 : 2);
}
void PS_TabCredentials::loadSettings (QSettings &s)	{
	s.beginGroup ("pushot");
	
	s.endGroup ();
	
	s.beginGroup ("custom");
	cbNeedsLogin->setChecked (s.value ("needsLogin", false).toBool ());
	leLogin->setText (s.value ("login", "").toString ());
	lePassword->setText (s.value ("password", "").toString ());
	cbSsl->setChecked (s.value ("useSsl", false).toBool ());
	s.endGroup ();
	
	leApiUrl->setText (s.value ("url").toString ());
	if (s.value ("mode", 1).toInt () == 1)
		rbPuShotServer->setChecked (true);
	else
		rbCustomServer->setChecked (true);
}

QString PS_TabCredentials::apiUrl ()	{
	return (rbPuShotServer->isChecked ()) ? "http://pushot.tuetuopay.fr/PS_API.php" : leApiUrl->text ();
}
QString PS_TabCredentials::username ()	{
	return leLogin->text ();
}
QString PS_TabCredentials::password ()	{
	return lePassword->text ();
}
bool PS_TabCredentials::useAuth ()	{
	return (rbPuShotServer->isChecked ()) ? true : ((cbNeedsLogin->isChecked ()) ? true : false);
}

PS_TabCredentials::~PS_TabCredentials () {}
