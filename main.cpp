/********************************************************************************
 *																				*
 *		main.cpp - Created 24/08/2014 23:13										*
 *																				*
 *		By Tuetuopay - http://tuetuopay.fr										*
 *																				*
 *		This file is part of PuShot.											*
 *																				*
 *		PuShot is free software: you can redistribute it and/or modify			*
 *		it under the terms of the GNU General Public License as published by	*
 *		the Free Software Foundation, either version 3 of the License, or		*
 *		(at your option) any later version.										*
 *																				*
 *		PuShot is distributed in the hope that it will be useful,				*
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of			*
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
 *		GNU General Public License for more details.							*
 *																				*
 *		You should have received a copy of the GNU General Public License		*
 *		along with PuShot.  If not, see <http://www.gnu.org/licenses/>.			*
 *																				*
 ********************************************************************************/

#include <iostream>

#include <QtCore>
#include <QtGui>

#include "PS_MainWindow.h"

int main (int argc, char **argv)	{
/*#ifdef Q_OS_MACX
    if ( QSysInfo::MacintoshVersion > QSysInfo::MV_10_8 )
    {
        // fix Mac OS X 10.9 (mavericks) font issue
        // https://bugreports.qt-project.org/browse/QTBUG-32789
        QFont::insertSubstitution(".Lucida Grande UI", "Lucida Grande");
    }
#endif*/
	QApplication app (argc, argv);
	app.setQuitOnLastWindowClosed (false);
    app.setAttribute (Qt::AA_UseHighDpiPixmaps);    // Enable Retina mode (High-DPI on non-Mac computers)
	
	QCoreApplication::setOrganizationName ("PuShot");
	QCoreApplication::setOrganizationDomain ("pushot.tuetuopay.fr");
	QCoreApplication::setApplicationName ("PuShot");
	
	PS_MainWindow *mainWindow = new PS_MainWindow ();
	mainWindow->show ();
	
	QObject::connect (&app, SIGNAL(aboutToQuit()), mainWindow, SLOT(atAppExit()));
	QObject::connect (mainWindow, SIGNAL(quit()), &app, SLOT(quit()));
	
	return app.exec ();
}
