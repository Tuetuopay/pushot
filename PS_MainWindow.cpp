/********************************************************************************
 *																				*
 *		PS_MainWindow.cpp - Created 24/08/2014 23:14							*
 *																				*
 *		By Tuetuopay - http://tuetuopay.fr										*
 *																				*
 *		This file is part of PuShot.											*
 *																				*
 *		PuShot is free software: you can redistribute it and/or modify			*
 *		it under the terms of the GNU General Public License as published by	*
 *		the Free Software Foundation, either version 3 of the License, or		*
 *		(at your option) any later version.										*
 *																				*
 *		PuShot is distributed in the hope that it will be useful,				*
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of			*
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
 *		GNU General Public License for more details.							*
 *																				*
 *		You should have received a copy of the GNU General Public License		*
 *		along with PuShot.  If not, see <http://www.gnu.org/licenses/>.			*
 *																				*
 ********************************************************************************/

#include <iostream>
#include <QtMultimedia>
#include "PS_MainWindow.h"
#include "PS_Utils.h"
#include "PS_NotificationMessage.h"

#define MAIN_WINDOW_TOP_PADDING		42
#define FILE_CHANGE_INTVAL			250

#ifdef Q_OS_MAC
#  define SOUND_EXT	"wav"
#else
#  define SOUND_EXT "wav"
#endif

PS_MainWindow::PS_MainWindow ()
: animCurve (QEasingCurve::OutCubic), scWindow (this), scSelection (this), scDesktop (this), scFile (this), scClipboard (this)	{
	setupUi (this);
	
	setUnifiedTitleAndToolBarOnMac (true);
	
	tabGeneral = new PS_TabGeneral (this);
	tabCredentials = new PS_TabCredentials (this);
	tabUpdates = new PS_TabUpdates (this);
	
	loadSettings ();
	
	tabCredentials->hide ();
	tabUpdates->hide ();
	
	currentTab = tabGeneral;
	
	verticalLayout->addWidget (tabGeneral);
	setMinimumHeight (tabGeneral->initHeight () + MAIN_WINDOW_TOP_PADDING);
	setMaximumHeight (tabGeneral->initHeight () + MAIN_WINDOW_TOP_PADDING);
	
	setFixedWidth (width ());
	
	updateGeometry ();
	
	/* Setting up the watching of the screenshots folder */
	watcher = new QFileSystemWatcher (QStringList (tabGeneral->screenshotsPath ()), this);
	lastDirectory = filesInDirectory ();
	QObject::connect (watcher, SIGNAL(directoryChanged(const QString&)), this, SLOT(on_watcher_directoryChanged(const QString&)));
	QObject::connect (tabGeneral, SIGNAL(screenshotsPathChanged(const QString&)), this, SLOT(on_tabGeneral_screenshotsPathChanged(const QString&)));
	
	// Those three functions are already native to OS X
	scSelection.setShortcut (QKeySequence (tr ("Ctrl+Shift+4")));
	scDesktop.setShortcut (QKeySequence (tr ("Ctrl+Shift+3")));
	scWindow.setShortcut (QKeySequence (tr ("Ctrl+Shift+2")));
#ifdef Q_OS_MAC
	scSelection.setEnabled (false);
	scDesktop.setEnabled (false);
	scWindow.setEnabled (false);
	scWindow.setShortcut (QKeySequence (tr ("Ctrl+Shift+4", "Space")));
#endif
	scFile.setShortcut (QKeySequence (tr ("Ctrl+Shift+u")));
	scClipboard.setShortcut (QKeySequence (tr ("Ctrl+Shift+c")));
	scClipboardExt.setShortcut (QKeySequence (tr ("Ctrl+Shift+Alt+c")));
	
	QObject::connect (&scSelection, SIGNAL(activated()), this, SLOT(shootSelection()));
	QObject::connect (&scDesktop, SIGNAL(activated()), this, SLOT(shootDesktop()));
	QObject::connect (&scWindow, SIGNAL(activated()), this, SLOT(shootWindow()));
	QObject::connect (&scFile, SIGNAL(activated()), this, SLOT(uploadFile()));
	QObject::connect (&scClipboard, SIGNAL(activated()), this, SLOT(uploadClipboard()));
	QObject::connect (&scClipboardExt, SIGNAL(activated()), this, SLOT(uploadClipboardExt()));
	
#ifdef Q_OS_MAC
	QDir::setCurrent (QApplication::applicationDirPath () + "/../Resources");
#endif
	
	setupTrayIcon ();
	
	uploading = false;
	disabled = false;
	inputFile = nullptr;
	askExt = false;
	iconHeldDown = false;
	
	selectionWidget = new PS_SelectionWidget ();
	QObject::connect (selectionWidget, SIGNAL(selectionFinished(QRect)), this, SLOT(selectionMade(QRect)));
	
#ifdef Q_OS_MAC
	registerListener (this);
	iconHeldDown = isNightModeEnabled ();
#endif
}

void PS_MainWindow::on_toolBar_actionTriggered (QAction *action)	{
	int oldHeight = height ();
	PS_Tab *nextTab = NULL;
	if (action == actionGeneral)
		nextTab = tabGeneral;
	else if (action == actionCredentials)
		nextTab = tabCredentials;
	else if (action == actionUpdates)
		nextTab = tabUpdates;
	
	actionGeneral->setChecked (false);
	actionCredentials->setChecked (false);
	actionUpdates->setChecked (false);
	action->setChecked (true);
	
	if (nextTab == currentTab)	return;
	
	verticalLayout->removeWidget (currentTab);
	verticalLayout->addWidget (nextTab);
	currentTab->hide ();
	nextTab->show ();
	
	animation = new QParallelAnimationGroup;
	
	QPropertyAnimation *animWindow = new QPropertyAnimation (this, "minimumHeight");
	animWindow->setDuration (500);
	animWindow->setEasingCurve (animCurve);
	animWindow->setStartValue (oldHeight);
	animWindow->setEndValue (nextTab->initHeight () + MAIN_WINDOW_TOP_PADDING);
	animation->addAnimation (animWindow);
	animWindow = new QPropertyAnimation (this, "maximumHeight");
	animWindow->setDuration (500);
	animWindow->setEasingCurve (animCurve);
	animWindow->setStartValue (oldHeight);
	animWindow->setEndValue (nextTab->initHeight () + MAIN_WINDOW_TOP_PADDING);
	animation->addAnimation (animWindow);
	
	//std::cout << "Animating to a new height of " << nextTab->initHeight () + MAIN_WINDOW_TOP_PADDING << std::endl;
	
	QObject::connect (animation, SIGNAL(finished()), this, SLOT(on_animation_finished()));
	animation->start ();
	
	currentTab = nextTab;
	
	saveSettings ();
}
void PS_MainWindow::atAppExit ()	{
	std::cout << "Window closed." << std::endl;
	saveSettings ();
}

void PS_MainWindow::on_animation_finished ()	{
	delete animation;
}
void PS_MainWindow::on_watcher_directoryChanged (const QString &path)	{
	QStringList dir = PS_Utils::listDiff (lastDirectory, filesInDirectory ());
	lastDirectory = filesInDirectory ();
	
	if (uploading || disabled)	return;
	
	if (dir.size () > 0)	{
		QString fileName;
		QRegExp rx (".+\\.png$");
		for (QString s : dir)
			if (rx.exactMatch (s))	{
				fileName = s;
				break;
			}
		if (fileName == "")	{
			std::cout << "Filename empty. Cancelling." << std::endl;
			return;
		}
		
		if (tabGeneral->screenshotsPath () + "/" + fileName == lastFile)
			return; /* Prevents uploading a file made by PuShot */
		
		std::cout << "Using file " << fileName.toStdString () << std::endl;
		if (!QFile::exists (tabGeneral->screenshotsPath () + "/" + fileName)) /* File deleted */
			return;
		
#ifdef Q_OS_WIN
		/* Give some time to the OS to save the file. */
		PS_Utils::delay (250);
#endif
		inputFile = new QFile (tabGeneral->screenshotsPath () + "/" + fileName);
		if (!inputFile->open (QIODevice::ReadOnly))	{
			QMessageBox::critical (this, "Error", "Fatal error : couldn't open file<br />" + tabGeneral->screenshotsPath () + "/" + fileName);
			return;
		}
		
		//QString hash = QCryptographicHash::hash (inputFile->readAll (), QCryptographicHash::Sha1);
		//if (hash == lastFileHash)	/* The event was triggered twice for the same file :/ */
		//	;//return;
		//lastFileHash = hash;
		
		/* Uploading the file */
		uploadData (inputFile->readAll (), fileName);
		
		lastFile = tabGeneral->screenshotsPath () + "/" + fileName;
	}
}

void PS_MainWindow::uploadData (const QByteArray &data, const QString &fileName)	{
	QHttpMultiPart *multiPart = new QHttpMultiPart (QHttpMultiPart::FormDataType);
	
	QHttpPart loginPart;
	loginPart.setHeader (QNetworkRequest::ContentDispositionHeader, QVariant ("form-data; name=\"login\""));
	loginPart.setBody (tabCredentials->username ().toLatin1 ());
	multiPart->append (loginPart);
	
	if (tabCredentials->useAuth ())	{
		QHttpPart passPart;
		passPart.setHeader (QNetworkRequest::ContentDispositionHeader, QVariant ("form-data; name=\"password\""));
		passPart.setBody (tabCredentials->password ().toLatin1 ());
		multiPart->append (passPart);
	}
	
	QHttpPart imagePart;
	imagePart.setHeader (QNetworkRequest::ContentDispositionHeader, QVariant ("form-data; name=\"media\"; filename=\"" + fileName + "\""));
	imagePart.setHeader (QNetworkRequest::ContentTypeHeader, QVariant ("image/png"));
	imagePart.setBody (data);
	// file->setParent (multiPart); // we cannot delete the file now, so delete it with the multiPart
	multiPart->append (imagePart);
	
	QNetworkRequest request (QUrl (tabCredentials->apiUrl ()));
	
	QNetworkReply *reply = manager.post (request, multiPart);
	multiPart->setParent (reply); // delete the multiPart with the reply
	
	QObject::connect (reply, SIGNAL(finished()), this, SLOT(on_upload_finished()));
	QObject::connect (reply, SIGNAL(uploadProgress(qint64,qint64)), this, SLOT(on_upload_progress(qint64,qint64)));
	QObject::connect (reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(on_upload_error(QNetworkReply::NetworkError)));
	
	std::cout << "Uploading file \"" << fileName.toStdString () << "\" ..." << std::endl;
}

void PS_MainWindow::on_upload_finished ()	{
	std::cout << "The upload finished !" << std::endl;
	QNetworkReply *sender = (QNetworkReply*)QObject::sender ();
	QString reply (sender->readAll ());
	
	uploading = false;
	
	if (reply.length () == 0)	{
		QMessageBox::critical (this, tr ("Error"), tr ("Empty server response."));
		return;
	}
	if (reply[0] != 'h')	{ /* If we don't have 'http' at the beginning ... */
		QMessageBox::critical (this, tr ("Error"), tr ("The server returned an error : ") + "<br>" + reply);
		return;
	}
	
	QClipboard *clipboard = QApplication::clipboard ();
	clipboard->setText (reply);
	
	if (tabGeneral->notifSound ())	{
		QSound::play ("complete1." SOUND_EXT);
	}
	if (tabGeneral->notifVisual ())	{
		systemTrayIcon->showMessage (tr ("Upload complete."), reply);
	}
	if (tabGeneral->openInBrowser ())	{
		QDesktopServices::openUrl (QUrl (reply));
	}

	if (inputFile)	{
		delete inputFile;
		inputFile = nullptr;
	}
#ifdef Q_OS_MAC
	systemTrayIcon->setIcon (QIcon (QString (":/icon/icon") + ((iconHeldDown) ? "-neg.png" : ".png")));
#endif
}
void PS_MainWindow::on_upload_progress (qint64 bytesSent, qint64 bytesTotal)	{
	int progressInPer45 = ((double)bytesSent / (double)bytesTotal) * 45;
	systemTrayIcon->setIcon (QIcon (QString (":/icon/icon-") + QString::number (progressInPer45) + ((iconHeldDown) ? "-neg.png" : ".png")));
}
void PS_MainWindow::on_upload_error (QNetworkReply::NetworkError code)	{
	std::cout << "Error code : " << code << std::endl;
}

QStringList PS_MainWindow::filesInDirectory () const	{
	QDir dir (tabGeneral->screenshotsPath ());
	return dir.entryList (QDir::Files | QDir::NoDotAndDotDot);
}

void PS_MainWindow::on_tabGeneral_screenshotsPathChanged (const QString &path)	{
	// QMessageBox::information (this, tr ("Screenshot path changed"), tr ("joes"));

	watcher->removePaths (watcher->directories ());
	watcher->addPath (path);
}

void PS_MainWindow::shootDesktop ()	{
	QDesktopWidget *desktop = QApplication::desktop ();
	QRect displayGeometry = PS_Utils::getDesktopGeometry ();
	QPixmap shot = QPixmap::grabWindow (desktop->winId (), displayGeometry.x (), displayGeometry.y (), displayGeometry.width(), displayGeometry.height());
	
	if (!tabGeneral->deleteFile ())	{
		lastFile = tabGeneral->screenshotsPath () + "/PuShot " + PS_Utils::getTime () + ".png";
		PS_Utils::savePixmap (shot, lastFile);
	}
	
	uploadData (PS_Utils::pixmap2byteArray (shot), QString ("PuShot ") + QString (PS_Utils::getTime ()) + ".png");
}
void PS_MainWindow::shootSelection ()	{
	selectionWidget->beginSelection ();
}
void PS_MainWindow::selectionMade (QRect selection)	{
	QDesktopWidget *desktop = QApplication::desktop ();
	QPixmap shot = QPixmap::grabWindow (desktop->winId(), selection.x (), selection.y (), selection.width (), selection.height ());
	
	if (!tabGeneral->deleteFile ())	{
		lastFile = tabGeneral->screenshotsPath () + "/PuShot " + PS_Utils::getTime () + ".png";
		PS_Utils::savePixmap (shot, lastFile);
	}
	
	uploadData (PS_Utils::pixmap2byteArray (shot), QString ("PuShot ") + QString (PS_Utils::getTime ()) + ".png");
}
void PS_MainWindow::shootWindow ()	{
	QDesktopWidget *desktop = QApplication::desktop ();
	QRect displayGeometry = PS_Utils::getActiveWindowGeometry ();
	// Just in case we compiled on a system that doesn't supports my code ...
	// E.G. OS X, which has this function built in natively anyway (⌘+⇧+4 then Space)
	if (displayGeometry.width () <= 0 || displayGeometry.height () <= 0)
		return;
	QPixmap shot = QPixmap::grabWindow (desktop->winId (), displayGeometry.x (), displayGeometry.y (), displayGeometry.width(), displayGeometry.height());
	
	if (!tabGeneral->deleteFile ())	{
		lastFile = tabGeneral->screenshotsPath () + "/PuShot " + PS_Utils::getTime () + ".png";
		PS_Utils::savePixmap (shot, lastFile);
	}
	
	uploadData (PS_Utils::pixmap2byteArray (shot), QString ("PuShot ") + QString (PS_Utils::getTime ()) + ".png");
}
void PS_MainWindow::uploadFile ()	{
	QString path = QFileDialog::getOpenFileName (this, tr ("Upload file"));
	
	if (path != "")	{
		QFile f (path);
		if (!f.open (QIODevice::ReadOnly))	{
			systemTrayIcon->showMessage (tr ("Error"), tr ("Upload failed : unable to open file &laqo; ") + path + " &raqo;.");
			std::cout << (tr ("Failed to upload : unable to open file &laqo; ") + path + " &raqo;.").toStdString () << std::endl;
			return;
		}
		QByteArray data = f.readAll ();
		
		QString file = path.split ("/").last ();
		
		//uploadData (data, "PuShot File " + PS_Utils::getTime () + file);
		uploadData (data, file);
	}
}
void PS_MainWindow::uploadClipboard ()	{
	const QClipboard *clipboard = QApplication::clipboard();
	const QMimeData *mimeData = clipboard->mimeData ();
	
	QString ext ("");
	QByteArray data;
	
	if (mimeData->hasImage ())	{
		ext = "png";
		data = PS_Utils::pixmap2byteArray (clipboard->pixmap ());
	}
	else if (mimeData->hasHtml ())	{
		ext = "html";
		data.append (mimeData->html ());
	}
	else if (mimeData->hasText ())	{
		ext = "txt";
		if (askExt)
			ext = QInputDialog::getText (this, tr ("Clipboard extension"), tr ("Please enter the extension of the text in your clipboard.<br>" \
																			   "Ex : py, cpp, html, ..."));
		if (ext == "")
			ext = "txt";
		data.append (mimeData->text ());
	}
	else if (mimeData->hasUrls ())	{
		QList<QUrl> urls = mimeData->urls ();
		QString txt ("");
		for (QUrl url : urls)
			txt += url.toString () + "\n";
		data.append (txt);
		ext = "txt";
	} else	{
		askExt = false;
		return;
	}
	askExt = false;
	
	uploadData (data, "PuShot Clipboard " + PS_Utils::getTime () + "." + ext);
	
	if (!tabGeneral->deleteFile ())	{
		lastFile = tabGeneral->screenshotsPath () + "/PuShot Clipboard " + PS_Utils::getTime () + "." + ext;
		PS_Utils::saveData (data, lastFile);
	}
}
void PS_MainWindow::uploadClipboardExt ()	{
	askExt = true;
	
	uploadClipboard ();
}
void PS_MainWindow::disableRequested ()	{
	disabled = actionDisablePuShot->isChecked ();
	
	if (disabled)	{
		scSelection.setDisabled ();
		scDesktop.setDisabled ();
		scWindow.setDisabled ();
		scFile.setDisabled ();
		scClipboard.setDisabled ();
	}
	else	{
		scSelection.setEnabled ();
		scDesktop.setEnabled ();
		scWindow.setEnabled ();
		scFile.setEnabled ();
		scClipboard.setEnabled ();
	}
}
void PS_MainWindow::preferencesRequested ()	{
	show ();
	raise ();
}
void PS_MainWindow::quitRequested ()	{
	emit quit ();
}

/* void PS_MainWindow::resizeEvent (QResizeEvent *event)	{
	std::cout << "Old size : (" << event->oldSize ().width () << ", " << event->oldSize ().height () << "); New size : (" << event->size ().width () << ", " << event->size ().height () << ")" << std::endl;
} */

void PS_MainWindow::saveSettings ()	{
	QSettings settings;
	settings.beginGroup ("general");
	tabGeneral->saveSettings (settings);
	settings.endGroup ();
	settings.beginGroup ("credentials");
	tabCredentials->saveSettings (settings);
	settings.endGroup ();
	settings.beginGroup ("updates");
	tabUpdates->saveSettings (settings);
	settings.endGroup ();
}
void PS_MainWindow::loadSettings ()	{
	QSettings settings;
	settings.beginGroup ("general");
	tabGeneral->loadSettings (settings);
	settings.endGroup ();
	settings.beginGroup ("credentials");
	tabCredentials->loadSettings (settings);
	settings.endGroup ();
	settings.beginGroup ("updates");
	tabUpdates->loadSettings (settings);
	settings.endGroup ();
}

void PS_MainWindow::setupTrayIcon ()	{
	systemTrayIcon = new QSystemTrayIcon (QIcon (":/icon/icon-neg.png"));
	systemTrayIcon->show ();
	
	actionMyAccount = new QAction (tr ("My Account"), this);
	actionSelectArea = new QAction (tr ("Select Area"), this);
	actionDesktopScreenshot = new QAction (tr ("Desktop Screenshot"), this);
	actionWindowScreenshot = new QAction (tr ("Window Screenshot"), this);
	actionUploadFile = new QAction (tr ("Upload File"), this);
	actionClipboard = new QAction (tr ("Upload Clipboard"), this);
	actionDisablePuShot = new QAction (tr ("Disable PuShot"), this);
	actionDisablePuShot->setCheckable (true);
	actionPreferences = new QAction (tr ("Preferences"), this);
	actionQuit = new QAction (tr ("Quit"), this);
	
	actionSelectArea->setShortcut (scSelection.shortcut ());
	actionDesktopScreenshot->setShortcut (scDesktop.shortcut ());
	actionWindowScreenshot->setShortcut (scWindow.shortcut ());
	actionUploadFile->setShortcut (scFile.shortcut ());
	actionClipboard->setShortcut (scClipboard.shortcut ());
	
#ifndef Q_OS_MAC
	QObject::connect (actionSelectArea, SIGNAL(triggered()), this, SLOT(shootSelection()));
	QObject::connect (actionDesktopScreenshot, SIGNAL(triggered()), this, SLOT(shootDesktop()));
	QObject::connect (actionWindowScreenshot, SIGNAL(triggered()), this, SLOT(shootWindow()));
#endif
	QObject::connect (actionUploadFile, SIGNAL(triggered()), this, SLOT(uploadFile()));
	QObject::connect (actionClipboard, SIGNAL(triggered()), this, SLOT(uploadClipboard()));
	QObject::connect (actionPreferences, SIGNAL(triggered()), this, SLOT(preferencesRequested()));
	QObject::connect (actionQuit, SIGNAL(triggered()), this, SLOT(quitRequested()));
	QObject::connect (actionDisablePuShot, SIGNAL(triggered()), this, SLOT(disableRequested()));
	
	menuTray = new QMenu (this);
	
	menuTray->addAction (actionMyAccount);
	menuTray->addSeparator ();
	menuTray->addAction (actionSelectArea);
	menuTray->addAction (actionDesktopScreenshot);
//#ifndef Q_OS_MAC
	menuTray->addAction (actionWindowScreenshot);
//#endif
	menuTray->addAction (actionUploadFile);
	menuTray->addAction (actionClipboard);
	menuTray->addSeparator ();
	menuTray->addAction (actionDisablePuShot);
	menuTray->addAction (actionPreferences);
	menuTray->addSeparator ();
	menuTray->addAction (actionQuit);
	
	systemTrayIcon->setContextMenu (menuTray);
}

#ifdef Q_OS_MAC
void PS_MainWindow::modeChanged (bool mode)	{
	std::cout << "Night mode : " << mode << std::endl;
	iconHeldDown = mode;
	
	systemTrayIcon->setIcon (QIcon (QString (":/icon/icon") + ((mode) ? "-neg.png" : ".png")));
}
#endif

PS_MainWindow::~PS_MainWindow ()	{
	
}
