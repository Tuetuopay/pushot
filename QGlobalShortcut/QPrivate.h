/****************************************************************************
 ** This file is derived from code bearing the following notice:
 ** The sole author of this file, Adam Higerd, has explicitly disclaimed all
 ** copyright interest and protection for the content within. This file has
 ** been placed in the public domain according to United States copyright
 ** statute and case law. In jurisdictions where this public domain dedication
 ** is not legally recognized, anyone who receives a copy of this file is
 ** permitted to use, modify, duplicate, and redistribute this file, in whole
 ** or in part, with no restrictions or conditions. In these jurisdictions,
 ** this file shall be copyright (C) 2006-2008 by Adam Higerd.
 ****************************************************************************/

#ifndef _Q_PRIVATE_H
#define _Q_PRIVATE_H

template <typename PUB>
class QPrivate	{
public:
	virtual ~QPrivate () {}
	inline void setPublic (PUB* pub)	{
		qxt_p_ptr = pub;
	}
	
protected:
	inline PUB& qxt_p ()	{
		return *qxt_p_ptr;
	}
	inline const PUB& qxt_p () const	{
		return *qxt_p_ptr;
	}
	inline PUB* qxt_ptr ()	{
		return qxt_p_ptr;
	}
	inline const PUB* qxt_ptr () const	{
		return qxt_p_ptr;
	}
	
private:
	PUB* qxt_p_ptr;
};

template <typename PUB, typename PVT>
class QPrivateInterface	{
	friend class QPrivate<PUB>;
public:
	QPrivateInterface ()	{
		pvt = new PVT;
	}
	~QPrivateInterface ()	{
		delete pvt;
	}
	
	inline void setPublic (PUB* pub)	{
		pvt->setPublic (pub);
	}
	inline PVT& operator() ()	{
		return *static_cast<PVT*>(pvt);
	}
	inline const PVT& operator() () const	{
		return *static_cast<PVT*>(pvt);
	}
	inline PVT * operator-> ()	{
		return static_cast<PVT*>(pvt);
	}
	inline const PVT * operator-> () const	{
		return static_cast<PVT*>(pvt);
	}
private:
	QPrivateInterface (const QPrivateInterface&)	{}
	QPrivateInterface& operator= (const QPrivateInterface&)	{}
	QPrivate<PUB>* pvt;
};

#endif /* defined (_Q_PRIVATE_H) */
