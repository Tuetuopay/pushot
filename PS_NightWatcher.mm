//
//  PS_NightWatcher.m
//  PuShot
//
//  Created by Alexis on 09/02/2015.
//
//

#import <Foundation/Foundation.h>

#import "PS_NightWatcher.h"

@interface _PS_NightWatcher : NSObject	{
}
@property PS_NightWatcher* callback;
-(id)init;
+(_PS_NightWatcher*)instance;
-(void)darkModeChanged:(NSNotification*)notif;
+(BOOL)nightModeOn;
-(void)registerCallback:(PS_NightWatcher*)c;
@end

@implementation _PS_NightWatcher

-(id)init	{
	[[NSDistributedNotificationCenter defaultCenter] addObserver:self selector:@selector(darkModeChanged:) name:@"AppleInterfaceThemeChangedNotification" object:nil];
	return self;
}

+(_PS_NightWatcher*)instance	{
	static _PS_NightWatcher *watcher = nil;
	if (watcher == nil)
		watcher = [[_PS_NightWatcher alloc] init];
	return watcher;
}

-(void)darkModeChanged:(NSNotification *)notif	{
	_callback->modeChanged ([_PS_NightWatcher nightModeOn] == YES);
}

+(BOOL)nightModeOn	{
	NSDictionary *dict = [[NSUserDefaults standardUserDefaults] persistentDomainForName:NSGlobalDomain];
	id style = [dict objectForKey:@"AppleInterfaceStyle"];
	return (style && [style isKindOfClass:[NSString class]] && NSOrderedSame == [style caseInsensitiveCompare:@"dark"]);
}

-(void)registerCallback:(PS_NightWatcher *)c	{
	_callback = c;
}

@end

bool isNightModeEnabled ()	{
	return [_PS_NightWatcher nightModeOn] == YES;
}
void registerListener (PS_NightWatcher *listener)	{
	[[_PS_NightWatcher instance] registerCallback:listener];
}
