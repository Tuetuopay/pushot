######################################################################
# Automatically generated by qmake (2.01a) lun. ao�t 25 12:05:50 2014
######################################################################

TEMPLATE = app
TARGET = PuShot
DEPENDPATH += .
INCLUDEPATH += .
QT += network widgets multimedia
CONFIG += c++11
QMAKE_CXXFLAGS += -std=c++11

SOUNDS.files = resources/complete1.wav \
               resources/complete2.wav
SOUNDS.path = $$OUT_PWD

unix {
    macx {
        LIBS += -framework Carbon \
                -framework Foundation
        SOUNDS.path = Contents/Resources
        QMAKE_BUNDLE_DATA += SOUNDS
        QMAKE_INFO_PLIST = Info.plist
        OBJECTIVE_SOURCES += PS_NightWatcher.m
        HEADERS += PS_NightWatcher.h
    } else {
        LIBS += -lX11
	    INSTALLS += SOUNDS
	}
}
win32 {
    INSTALLS += SOUNDS
}

# Input
HEADERS += PS_MainWindow.h \
           PS_Tab.h \
           PS_TabCredentials.h \
           PS_TabGeneral.h \
           PS_TabUpdates.h \
           PS_Utils.h \
           QGlobalShortcut/QGlobalShortcut.h \
           QGlobalShortcut/QGlobalShortcut_p.h \
           QGlobalShortcut/QPrivate.h \
           PS_SelectionWidget.h 
FORMS += MainWindow.ui TabCredentials.ui TabGeneral.ui TabUpdates.ui
SOURCES += main.cpp \
           PS_MainWindow.cpp \
           PS_TabCredentials.cpp \
           PS_TabGeneral.cpp \
           PS_TabUpdates.cpp \
           PS_Utils.cpp \
           QGlobalShortcut/QGlobalShortcut.cpp \
           QGlobalShortcut/QGlobalShortcut_p.cpp \
           PS_SelectionWidget.cpp
RESOURCES += resources.qrc

unix {
    macx {
        SOURCES += QGlobalShortcut/QGlobalShortcut_mac.cpp
    } else {
        SOURCES += QGlobalShortcut/QGlobalShortcut_x11.cpp
    }
}
 
win32 {
    SOURCES += QGlobalShortcut/QGlobalShortcut_win.cpp
}

