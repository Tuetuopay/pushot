/****************************************************************************
** Meta object code from reading C++ file 'PS_MainWindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "PS_MainWindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'PS_MainWindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_PS_MainWindow_t {
    QByteArrayData data[29];
    char stringdata[435];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PS_MainWindow_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PS_MainWindow_t qt_meta_stringdata_PS_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 13), // "PS_MainWindow"
QT_MOC_LITERAL(1, 14, 4), // "quit"
QT_MOC_LITERAL(2, 19, 0), // ""
QT_MOC_LITERAL(3, 20, 26), // "on_toolBar_actionTriggered"
QT_MOC_LITERAL(4, 47, 8), // "QAction*"
QT_MOC_LITERAL(5, 56, 6), // "action"
QT_MOC_LITERAL(6, 63, 9), // "atAppExit"
QT_MOC_LITERAL(7, 73, 21), // "on_animation_finished"
QT_MOC_LITERAL(8, 95, 27), // "on_watcher_directoryChanged"
QT_MOC_LITERAL(9, 123, 4), // "path"
QT_MOC_LITERAL(10, 128, 18), // "on_upload_finished"
QT_MOC_LITERAL(11, 147, 18), // "on_upload_progress"
QT_MOC_LITERAL(12, 166, 9), // "bytesSent"
QT_MOC_LITERAL(13, 176, 10), // "bytesTotal"
QT_MOC_LITERAL(14, 187, 15), // "on_upload_error"
QT_MOC_LITERAL(15, 203, 27), // "QNetworkReply::NetworkError"
QT_MOC_LITERAL(16, 231, 4), // "code"
QT_MOC_LITERAL(17, 236, 36), // "on_tabGeneral_screenshotsPath..."
QT_MOC_LITERAL(18, 273, 12), // "shootDesktop"
QT_MOC_LITERAL(19, 286, 14), // "shootSelection"
QT_MOC_LITERAL(20, 301, 13), // "selectionMade"
QT_MOC_LITERAL(21, 315, 9), // "selection"
QT_MOC_LITERAL(22, 325, 11), // "shootWindow"
QT_MOC_LITERAL(23, 337, 10), // "uploadFile"
QT_MOC_LITERAL(24, 348, 15), // "uploadClipboard"
QT_MOC_LITERAL(25, 364, 18), // "uploadClipboardExt"
QT_MOC_LITERAL(26, 383, 16), // "disableRequested"
QT_MOC_LITERAL(27, 400, 20), // "preferencesRequested"
QT_MOC_LITERAL(28, 421, 13) // "quitRequested"

    },
    "PS_MainWindow\0quit\0\0on_toolBar_actionTriggered\0"
    "QAction*\0action\0atAppExit\0"
    "on_animation_finished\0on_watcher_directoryChanged\0"
    "path\0on_upload_finished\0on_upload_progress\0"
    "bytesSent\0bytesTotal\0on_upload_error\0"
    "QNetworkReply::NetworkError\0code\0"
    "on_tabGeneral_screenshotsPathChanged\0"
    "shootDesktop\0shootSelection\0selectionMade\0"
    "selection\0shootWindow\0uploadFile\0"
    "uploadClipboard\0uploadClipboardExt\0"
    "disableRequested\0preferencesRequested\0"
    "quitRequested"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PS_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  109,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    1,  110,    2, 0x0a /* Public */,
       6,    0,  113,    2, 0x0a /* Public */,
       7,    0,  114,    2, 0x0a /* Public */,
       8,    1,  115,    2, 0x0a /* Public */,
      10,    0,  118,    2, 0x0a /* Public */,
      11,    2,  119,    2, 0x0a /* Public */,
      14,    1,  124,    2, 0x0a /* Public */,
      17,    1,  127,    2, 0x0a /* Public */,
      18,    0,  130,    2, 0x0a /* Public */,
      19,    0,  131,    2, 0x0a /* Public */,
      20,    1,  132,    2, 0x0a /* Public */,
      22,    0,  135,    2, 0x0a /* Public */,
      23,    0,  136,    2, 0x0a /* Public */,
      24,    0,  137,    2, 0x0a /* Public */,
      25,    0,  138,    2, 0x0a /* Public */,
      26,    0,  139,    2, 0x0a /* Public */,
      27,    0,  140,    2, 0x0a /* Public */,
      28,    0,  141,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void,
    QMetaType::Void, QMetaType::LongLong, QMetaType::LongLong,   12,   13,
    QMetaType::Void, 0x80000000 | 15,   16,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QRect,   21,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void PS_MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        PS_MainWindow *_t = static_cast<PS_MainWindow *>(_o);
        switch (_id) {
        case 0: _t->quit(); break;
        case 1: _t->on_toolBar_actionTriggered((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 2: _t->atAppExit(); break;
        case 3: _t->on_animation_finished(); break;
        case 4: _t->on_watcher_directoryChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: _t->on_upload_finished(); break;
        case 6: _t->on_upload_progress((*reinterpret_cast< qint64(*)>(_a[1])),(*reinterpret_cast< qint64(*)>(_a[2]))); break;
        case 7: _t->on_upload_error((*reinterpret_cast< QNetworkReply::NetworkError(*)>(_a[1]))); break;
        case 8: _t->on_tabGeneral_screenshotsPathChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 9: _t->shootDesktop(); break;
        case 10: _t->shootSelection(); break;
        case 11: _t->selectionMade((*reinterpret_cast< QRect(*)>(_a[1]))); break;
        case 12: _t->shootWindow(); break;
        case 13: _t->uploadFile(); break;
        case 14: _t->uploadClipboard(); break;
        case 15: _t->uploadClipboardExt(); break;
        case 16: _t->disableRequested(); break;
        case 17: _t->preferencesRequested(); break;
        case 18: _t->quitRequested(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAction* >(); break;
            }
            break;
        case 7:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QNetworkReply::NetworkError >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (PS_MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PS_MainWindow::quit)) {
                *result = 0;
            }
        }
    }
}

const QMetaObject PS_MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_PS_MainWindow.data,
      qt_meta_data_PS_MainWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *PS_MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PS_MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_PS_MainWindow.stringdata))
        return static_cast<void*>(const_cast< PS_MainWindow*>(this));
    if (!strcmp(_clname, "Ui_MainWindow"))
        return static_cast< Ui_MainWindow*>(const_cast< PS_MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int PS_MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    }
    return _id;
}

// SIGNAL 0
void PS_MainWindow::quit()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
