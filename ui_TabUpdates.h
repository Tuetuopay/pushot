/********************************************************************************
** Form generated from reading UI file 'TabUpdates.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TABUPDATES_H
#define UI_TABUPDATES_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TabUpdates
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *lblStatus;
    QPushButton *btnCheckUpdates;

    void setupUi(QWidget *TabUpdates)
    {
        if (TabUpdates->objectName().isEmpty())
            TabUpdates->setObjectName(QStringLiteral("TabUpdates"));
        TabUpdates->resize(400, 71);
        verticalLayout = new QVBoxLayout(TabUpdates);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        lblStatus = new QLabel(TabUpdates);
        lblStatus->setObjectName(QStringLiteral("lblStatus"));
        lblStatus->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(lblStatus);

        btnCheckUpdates = new QPushButton(TabUpdates);
        btnCheckUpdates->setObjectName(QStringLiteral("btnCheckUpdates"));

        verticalLayout->addWidget(btnCheckUpdates);


        retranslateUi(TabUpdates);

        QMetaObject::connectSlotsByName(TabUpdates);
    } // setupUi

    void retranslateUi(QWidget *TabUpdates)
    {
        TabUpdates->setWindowTitle(QApplication::translate("TabUpdates", "Form", 0));
        lblStatus->setText(QApplication::translate("TabUpdates", "No updates found. (last checked : xxx)", 0));
        btnCheckUpdates->setText(QApplication::translate("TabUpdates", "Check for updates", 0));
    } // retranslateUi

};

namespace Ui {
    class TabUpdates: public Ui_TabUpdates {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TABUPDATES_H
