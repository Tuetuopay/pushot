/********************************************************************************
** Form generated from reading UI file 'TabGeneral.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TABGENERAL_H
#define UI_TABGENERAL_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TabGeneral
{
public:
    QVBoxLayout *verticalLayout;
    QGroupBox *gbGeneral;
    QVBoxLayout *verticalLayout_2;
    QCheckBox *cbStartOnBoot;
    QLabel *lblScreenshotPah;
    QHBoxLayout *layScreenshotPath;
    QLineEdit *leScreenshotPath;
    QToolButton *btnBrowse;
    QGroupBox *gbOnUpload;
    QVBoxLayout *verticalLayout_3;
    QCheckBox *cbNotifVisual;
    QCheckBox *cbNotifSound;
    QCheckBox *cbCopyClipboard;
    QCheckBox *cbOpenInBrowser;
    QCheckBox *cbDelete;
    QGroupBox *gbKeyboard;
    QVBoxLayout *verticalLayout_4;
    QCheckBox *cbSystemKeys;

    void setupUi(QWidget *TabGeneral)
    {
        if (TabGeneral->objectName().isEmpty())
            TabGeneral->setObjectName(QStringLiteral("TabGeneral"));
        TabGeneral->resize(400, 315);
        TabGeneral->setMinimumSize(QSize(0, 242));
        verticalLayout = new QVBoxLayout(TabGeneral);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        gbGeneral = new QGroupBox(TabGeneral);
        gbGeneral->setObjectName(QStringLiteral("gbGeneral"));
        verticalLayout_2 = new QVBoxLayout(gbGeneral);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        cbStartOnBoot = new QCheckBox(gbGeneral);
        cbStartOnBoot->setObjectName(QStringLiteral("cbStartOnBoot"));
        cbStartOnBoot->setChecked(true);

        verticalLayout_2->addWidget(cbStartOnBoot);

        lblScreenshotPah = new QLabel(gbGeneral);
        lblScreenshotPah->setObjectName(QStringLiteral("lblScreenshotPah"));

        verticalLayout_2->addWidget(lblScreenshotPah);

        layScreenshotPath = new QHBoxLayout();
        layScreenshotPath->setObjectName(QStringLiteral("layScreenshotPath"));
        leScreenshotPath = new QLineEdit(gbGeneral);
        leScreenshotPath->setObjectName(QStringLiteral("leScreenshotPath"));

        layScreenshotPath->addWidget(leScreenshotPath);

        btnBrowse = new QToolButton(gbGeneral);
        btnBrowse->setObjectName(QStringLiteral("btnBrowse"));

        layScreenshotPath->addWidget(btnBrowse);


        verticalLayout_2->addLayout(layScreenshotPath);


        verticalLayout->addWidget(gbGeneral);

        gbOnUpload = new QGroupBox(TabGeneral);
        gbOnUpload->setObjectName(QStringLiteral("gbOnUpload"));
        verticalLayout_3 = new QVBoxLayout(gbOnUpload);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        cbNotifVisual = new QCheckBox(gbOnUpload);
        cbNotifVisual->setObjectName(QStringLiteral("cbNotifVisual"));
        cbNotifVisual->setChecked(true);

        verticalLayout_3->addWidget(cbNotifVisual);

        cbNotifSound = new QCheckBox(gbOnUpload);
        cbNotifSound->setObjectName(QStringLiteral("cbNotifSound"));
        cbNotifSound->setChecked(true);

        verticalLayout_3->addWidget(cbNotifSound);

        cbCopyClipboard = new QCheckBox(gbOnUpload);
        cbCopyClipboard->setObjectName(QStringLiteral("cbCopyClipboard"));
        cbCopyClipboard->setChecked(true);

        verticalLayout_3->addWidget(cbCopyClipboard);

        cbOpenInBrowser = new QCheckBox(gbOnUpload);
        cbOpenInBrowser->setObjectName(QStringLiteral("cbOpenInBrowser"));

        verticalLayout_3->addWidget(cbOpenInBrowser);

        cbDelete = new QCheckBox(gbOnUpload);
        cbDelete->setObjectName(QStringLiteral("cbDelete"));

        verticalLayout_3->addWidget(cbDelete);


        verticalLayout->addWidget(gbOnUpload);

        gbKeyboard = new QGroupBox(TabGeneral);
        gbKeyboard->setObjectName(QStringLiteral("gbKeyboard"));
        gbKeyboard->setCheckable(false);
        verticalLayout_4 = new QVBoxLayout(gbKeyboard);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        cbSystemKeys = new QCheckBox(gbKeyboard);
        cbSystemKeys->setObjectName(QStringLiteral("cbSystemKeys"));
        cbSystemKeys->setChecked(true);

        verticalLayout_4->addWidget(cbSystemKeys);


        verticalLayout->addWidget(gbKeyboard);


        retranslateUi(TabGeneral);

        QMetaObject::connectSlotsByName(TabGeneral);
    } // setupUi

    void retranslateUi(QWidget *TabGeneral)
    {
        TabGeneral->setWindowTitle(QApplication::translate("TabGeneral", "Form", 0));
        gbGeneral->setTitle(QApplication::translate("TabGeneral", "General", 0));
        cbStartOnBoot->setText(QApplication::translate("TabGeneral", "Start on system boot", 0));
        lblScreenshotPah->setText(QApplication::translate("TabGeneral", "Screenshots folder :", 0));
        leScreenshotPath->setPlaceholderText(QApplication::translate("TabGeneral", "~/Desktop/", 0));
        btnBrowse->setText(QApplication::translate("TabGeneral", "...", 0));
        gbOnUpload->setTitle(QApplication::translate("TabGeneral", "On upload completion", 0));
        cbNotifVisual->setText(QApplication::translate("TabGeneral", "Show a visual notification", 0));
        cbNotifSound->setText(QApplication::translate("TabGeneral", "Play a sound", 0));
        cbCopyClipboard->setText(QApplication::translate("TabGeneral", "Copy link to clipboard", 0));
        cbOpenInBrowser->setText(QApplication::translate("TabGeneral", "Open in browser", 0));
        cbDelete->setText(QApplication::translate("TabGeneral", "Delete from disk", 0));
        gbKeyboard->setTitle(QApplication::translate("TabGeneral", "Keyboard Shortcuts", 0));
        cbSystemKeys->setText(QApplication::translate("TabGeneral", "Use system shortcuts (if the system have some)", 0));
    } // retranslateUi

};

namespace Ui {
    class TabGeneral: public Ui_TabGeneral {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TABGENERAL_H
