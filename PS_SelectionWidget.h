/********************************************************************************
 *																				*
 *		PS_SelectionWidget.h - Created 01/09/2014 19:32							*
 *																				*
 *		By Tuetuopay - http://tuetuopay.fr										*
 *																				*
 *		This file is part of PuShot.											*
 *																				*
 *		PuShot is free software: you can redistribute it and/or modify			*
 *		it under the terms of the GNU General Public License as published by	*
 *		the Free Software Foundation, either version 3 of the License, or		*
 *		(at your option) any later version.										*
 *																				*
 *		PuShot is distributed in the hope that it will be useful,				*
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of			*
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
 *		GNU General Public License for more details.							*
 *																				*
 *		You should have received a copy of the GNU General Public License		*
 *		along with PuShot.  If not, see <http://www.gnu.org/licenses/>.			*
 *																				*
 ********************************************************************************/

#ifndef _PS_SELECTION_WIDGET_H
#define _PS_SELECTION_WIDGET_H
#include <QtWidgets>
#include <QtGui>

class PS_SelectionWidget : public QWidget	{
	Q_OBJECT
	
public:
	PS_SelectionWidget ();
	~PS_SelectionWidget ();
	
public slots:
	void beginSelection ();
	
signals:
	void selectionFinished (QRect selection);
	
protected:
	virtual void mousePressEvent (QMouseEvent *event);
	virtual void mouseReleaseEvent (QMouseEvent *event);
	virtual void keyPressEvent (QKeyEvent *event);
	virtual void mouseMoveEvent (QMouseEvent *event);
	
private:
	QFrame *frame;
	QPoint firstCorner;
	
	bool selecting;
};

#endif /* defined(_PS_SelectionWidget_H) */
