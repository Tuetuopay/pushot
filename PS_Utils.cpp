/********************************************************************************
 *																				*
 *		PS_Utils.h - Created 25/08/2014 13:15									*
 *																				*
 *		By Tuetuopay - http://tuetuopay.fr										*
 *																				*
 *		This file is part of PuShot.											*
 *																				*
 *		PuShot is free software: you can redistribute it and/or modify			*
 *		it under the terms of the GNU General Public License as published by	*
 *		the Free Software Foundation, either version 3 of the License, or		*
 *		(at your option) any later version.										*
 *																				*
 *		PuShot is distributed in the hope that it will be useful,				*
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of			*
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
 *		GNU General Public License for more details.							*
 *																				*
 *		You should have received a copy of the GNU General Public License		*
 *		along with PuShot.  If not, see <http://www.gnu.org/licenses/>.			*
 *																				*
 ********************************************************************************/

#include "PS_Utils.h"

#include <ctime>

QString PS_Utils::getDesktopLocation ()	{
	return QStandardPaths::standardLocations (QStandardPaths::DesktopLocation)[0];
}
QString PS_Utils::getPicturesLocation ()	{
	return QStandardPaths::standardLocations (QStandardPaths::PicturesLocation)[0];
}

QStringList PS_Utils::listDiff (const QStringList &list1, const QStringList &list2)	{
	QStringList ret;
	for (QString s : list1)	{
		if (!list2.contains (s))
			ret << s;
	}
	for (QString s : list2)	{
		if (!list1.contains (s))
			ret << s;
	}
	return ret;
}

void PS_Utils::delay (int msec)	{
	QTime dieTime = QTime::currentTime ().addMSecs (msec);
	while (QTime::currentTime () < dieTime)
		QCoreApplication::processEvents (QEventLoop::AllEvents, 100);
}

QByteArray PS_Utils::pixmap2byteArray (const QPixmap &pixmap)	{
	QByteArray bArray;
	QBuffer buffer (&bArray);
	buffer.open (QIODevice::WriteOnly);
	pixmap.save (&buffer, "PNG");
	return bArray;
}

template<class T>
std::string to_string_2 (const T& v)	{
	std::string ret = QString::number (v).toStdString ();
	if (ret.length () == 1)
		ret = "0" + ret;
	return ret;
}
QString PS_Utils::getTime ()	{
	time_t t = time (nullptr);
    struct tm * now = localtime (&t);
	std::string res;
	res += to_string_2 (now->tm_mday);
	res += "-" + to_string_2 (now->tm_mon + 1);
	res += "-" + to_string_2 (now->tm_year + 1900);
	res += " " + to_string_2 (now->tm_hour);
	res += "." + to_string_2 (now->tm_min);
	res += "." + to_string_2 (now->tm_sec);
	// delete now;
	return QString (res.c_str ());
}
bool PS_Utils::savePixmap (const QPixmap &pixmap, const QString &filename)	{
	return pixmap.save (filename);
}
bool PS_Utils::saveData (const QByteArray &data, const QString &filename)	{
	QFile f (filename);
	if (!f.open (QIODevice::WriteOnly))
		return false;
	return f.write (data) != 0;
}

QRect PS_Utils::getDesktopGeometry ()	{
	QDesktopWidget *desktop = QApplication::desktop ();
	int screens = desktop->screenCount ();
	QRect displayGeometry;
	for (int i = 0; i < screens; i++)
		displayGeometry = displayGeometry.united (desktop->screen (i)->geometry ());
	return displayGeometry;
}

QRect PS_Utils::getRect (QPoint p1, QPoint p2)	{
	int buf;
	if (p1.x () > p2.x ())	{
		buf = p1.x ();
		p1.setX (p2.x ());
		p2.setX (buf);
	}
	if (p1.y () > p2.y ())	{
		buf = p1.y ();
		p1.setY (p2.y ());
		p2.setY (buf);
	}
	
	return QRect (p1, p2);
}

#ifdef Q_OS_WIN
#include <qt_windows.h>

QRect PS_Utils::getActiveWindowGeometry ()	{
	RECT rect;
	GetWindowRect (GetForegroundWindow (), &rect);

	return QRect (QPoint (rect.left, rect.top), QPoint (rect.right, rect.bottom));
}

#else
QRect PS_Utils::getActiveWindowGeometry ()	{
	return QRect ();
}
#endif
