/********************************************************************************
** Form generated from reading UI file 'TabCredentials.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TABCREDENTIALS_H
#define UI_TABCREDENTIALS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TabCredentials
{
public:
    QVBoxLayout *verticalLayout;
    QGroupBox *gbDetails;
    QVBoxLayout *verticalLayout_2;
    QFormLayout *layCustomCredentials;
    QLabel *lblLogin;
    QLineEdit *leLogin;
    QLabel *lblPassword;
    QLineEdit *lePassword;
    QRadioButton *rbPuShotServer;
    QGroupBox *gbPSAccount;
    QFormLayout *formLayout;
    QLabel *lblAccount;
    QLabel *lblPSLogin;
    QPushButton *btnViewAccount;
    QPushButton *btnLogout;
    QRadioButton *rbCustomServer;
    QGroupBox *gbCustomServer;
    QVBoxLayout *verticalLayout_3;
    QCheckBox *cbNeedsLogin;
    QCheckBox *cbSsl;
    QHBoxLayout *layApiUrl;
    QLabel *lblApiUrl;
    QLineEdit *leApiUrl;

    void setupUi(QWidget *TabCredentials)
    {
        if (TabCredentials->objectName().isEmpty())
            TabCredentials->setObjectName(QStringLiteral("TabCredentials"));
        TabCredentials->resize(400, 366);
        verticalLayout = new QVBoxLayout(TabCredentials);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        gbDetails = new QGroupBox(TabCredentials);
        gbDetails->setObjectName(QStringLiteral("gbDetails"));
        verticalLayout_2 = new QVBoxLayout(gbDetails);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        layCustomCredentials = new QFormLayout();
        layCustomCredentials->setObjectName(QStringLiteral("layCustomCredentials"));
        layCustomCredentials->setFieldGrowthPolicy(QFormLayout::FieldsStayAtSizeHint);
        lblLogin = new QLabel(gbDetails);
        lblLogin->setObjectName(QStringLiteral("lblLogin"));

        layCustomCredentials->setWidget(0, QFormLayout::LabelRole, lblLogin);

        leLogin = new QLineEdit(gbDetails);
        leLogin->setObjectName(QStringLiteral("leLogin"));

        layCustomCredentials->setWidget(0, QFormLayout::FieldRole, leLogin);

        lblPassword = new QLabel(gbDetails);
        lblPassword->setObjectName(QStringLiteral("lblPassword"));

        layCustomCredentials->setWidget(1, QFormLayout::LabelRole, lblPassword);

        lePassword = new QLineEdit(gbDetails);
        lePassword->setObjectName(QStringLiteral("lePassword"));
        lePassword->setEchoMode(QLineEdit::Password);

        layCustomCredentials->setWidget(1, QFormLayout::FieldRole, lePassword);


        verticalLayout_2->addLayout(layCustomCredentials);

        rbPuShotServer = new QRadioButton(gbDetails);
        rbPuShotServer->setObjectName(QStringLiteral("rbPuShotServer"));
        rbPuShotServer->setChecked(true);

        verticalLayout_2->addWidget(rbPuShotServer);

        gbPSAccount = new QGroupBox(gbDetails);
        gbPSAccount->setObjectName(QStringLiteral("gbPSAccount"));
        formLayout = new QFormLayout(gbPSAccount);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        lblAccount = new QLabel(gbPSAccount);
        lblAccount->setObjectName(QStringLiteral("lblAccount"));

        formLayout->setWidget(0, QFormLayout::LabelRole, lblAccount);

        lblPSLogin = new QLabel(gbPSAccount);
        lblPSLogin->setObjectName(QStringLiteral("lblPSLogin"));

        formLayout->setWidget(0, QFormLayout::FieldRole, lblPSLogin);

        btnViewAccount = new QPushButton(gbPSAccount);
        btnViewAccount->setObjectName(QStringLiteral("btnViewAccount"));

        formLayout->setWidget(1, QFormLayout::LabelRole, btnViewAccount);

        btnLogout = new QPushButton(gbPSAccount);
        btnLogout->setObjectName(QStringLiteral("btnLogout"));

        formLayout->setWidget(1, QFormLayout::FieldRole, btnLogout);


        verticalLayout_2->addWidget(gbPSAccount);

        rbCustomServer = new QRadioButton(gbDetails);
        rbCustomServer->setObjectName(QStringLiteral("rbCustomServer"));

        verticalLayout_2->addWidget(rbCustomServer);

        gbCustomServer = new QGroupBox(gbDetails);
        gbCustomServer->setObjectName(QStringLiteral("gbCustomServer"));
        gbCustomServer->setEnabled(false);
        verticalLayout_3 = new QVBoxLayout(gbCustomServer);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        cbNeedsLogin = new QCheckBox(gbCustomServer);
        cbNeedsLogin->setObjectName(QStringLiteral("cbNeedsLogin"));

        verticalLayout_3->addWidget(cbNeedsLogin);

        cbSsl = new QCheckBox(gbCustomServer);
        cbSsl->setObjectName(QStringLiteral("cbSsl"));
        cbSsl->setEnabled(false);

        verticalLayout_3->addWidget(cbSsl);

        layApiUrl = new QHBoxLayout();
        layApiUrl->setObjectName(QStringLiteral("layApiUrl"));
        lblApiUrl = new QLabel(gbCustomServer);
        lblApiUrl->setObjectName(QStringLiteral("lblApiUrl"));

        layApiUrl->addWidget(lblApiUrl);

        leApiUrl = new QLineEdit(gbCustomServer);
        leApiUrl->setObjectName(QStringLiteral("leApiUrl"));

        layApiUrl->addWidget(leApiUrl);


        verticalLayout_3->addLayout(layApiUrl);


        verticalLayout_2->addWidget(gbCustomServer);


        verticalLayout->addWidget(gbDetails);


        retranslateUi(TabCredentials);
        QObject::connect(rbPuShotServer, SIGNAL(toggled(bool)), gbPSAccount, SLOT(setEnabled(bool)));
        QObject::connect(rbCustomServer, SIGNAL(toggled(bool)), gbCustomServer, SLOT(setEnabled(bool)));
        QObject::connect(cbNeedsLogin, SIGNAL(toggled(bool)), cbSsl, SLOT(setEnabled(bool)));

        QMetaObject::connectSlotsByName(TabCredentials);
    } // setupUi

    void retranslateUi(QWidget *TabCredentials)
    {
        TabCredentials->setWindowTitle(QApplication::translate("TabCredentials", "Form", 0));
        gbDetails->setTitle(QApplication::translate("TabCredentials", "Details", 0));
        lblLogin->setText(QApplication::translate("TabCredentials", "Login", 0));
        leLogin->setPlaceholderText(QApplication::translate("TabCredentials", "Account Login", 0));
        lblPassword->setText(QApplication::translate("TabCredentials", "Password", 0));
        lePassword->setPlaceholderText(QApplication::translate("TabCredentials", "Account Password", 0));
        rbPuShotServer->setText(QApplication::translate("TabCredentials", "Use PuShot servers", 0));
        gbPSAccount->setTitle(QApplication::translate("TabCredentials", "PuShot Account", 0));
        lblAccount->setText(QApplication::translate("TabCredentials", "Account", 0));
        lblPSLogin->setText(QApplication::translate("TabCredentials", "account_login", 0));
        btnViewAccount->setText(QApplication::translate("TabCredentials", "View Account", 0));
        btnLogout->setText(QApplication::translate("TabCredentials", "Logout", 0));
        rbCustomServer->setText(QApplication::translate("TabCredentials", "Use custom server", 0));
        gbCustomServer->setTitle(QApplication::translate("TabCredentials", "Custom Server", 0));
        cbNeedsLogin->setText(QApplication::translate("TabCredentials", "Needs login", 0));
        cbSsl->setText(QApplication::translate("TabCredentials", "SSL", 0));
        lblApiUrl->setText(QApplication::translate("TabCredentials", "API URL", 0));
        leApiUrl->setPlaceholderText(QApplication::translate("TabCredentials", "http(s)://pushot.me/...", 0));
    } // retranslateUi

};

namespace Ui {
    class TabCredentials: public Ui_TabCredentials {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TABCREDENTIALS_H
