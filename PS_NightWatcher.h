//
//  PS_NightWatcher.h
//  PuShot
//
//  Created by Alexis on 09/02/2015.
//
//

#ifndef PS_NIGHT_WATCHER_H
#define PS_NIGHT_WATCHER_H

class PS_NightWatcher	{
public:
	PS_NightWatcher () {}
	
	virtual void modeChanged (bool mode) = 0;
};

bool isNightModeEnabled ();
void registerListener (PS_NightWatcher *listener);

#endif
