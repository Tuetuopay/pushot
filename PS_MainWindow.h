/********************************************************************************
 *																				*
 *		PS_MainWindow.h - Created 24/08/2014 23:15								*
 *																				*
 *		By Tuetuopay - http://tuetuopay.fr										*
 *																				*
 *		This file is part of PuShot.											*
 *																				*
 *		PuShot is free software: you can redistribute it and/or modify			*
 *		it under the terms of the GNU General Public License as published by	*
 *		the Free Software Foundation, either version 3 of the License, or		*
 *		(at your option) any later version.										*
 *																				*
 *		PuShot is distributed in the hope that it will be useful,				*
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of			*
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
 *		GNU General Public License for more details.							*
 *																				*
 *		You should have received a copy of the GNU General Public License		*
 *		along with PuShot.  If not, see <http://www.gnu.org/licenses/>.			*
 *																				*
 ********************************************************************************/

#ifndef _PS_MAIN_WINDOW_H
#define _PS_MAIN_WINDOW_H

#include <QtGui>
#include <QtWidgets>
#include <QtNetwork>
#include <QSystemTrayIcon.h>
#include "ui_MainWindow.h"

#include "PS_TabGeneral.h"
#include "PS_TabCredentials.h"
#include "PS_TabUpdates.h"

#include "PS_SelectionWidget.h"

#ifdef Q_OS_MAC
# include "PS_NightWatcher.h"
#endif

#include "QGlobalShortcut/QGlobalShortcut.h"

class PS_MainWindow : public QMainWindow, protected Ui_MainWindow
#ifdef Q_OS_MAC
, private PS_NightWatcher
#endif
{
	Q_OBJECT
	
public:
	PS_MainWindow ();
	~PS_MainWindow ();
	
	void saveSettings ();
	void loadSettings ();
	
#ifdef Q_OS_MAC
	void modeChanged (bool mode);
#endif
	
public slots:
	void on_toolBar_actionTriggered (QAction *action);
	void atAppExit ();
	
	void on_animation_finished ();
	void on_watcher_directoryChanged (const QString &path);
	
	void on_upload_finished ();
	void on_upload_progress (qint64 bytesSent, qint64 bytesTotal);
	void on_upload_error (QNetworkReply::NetworkError code);

	void on_tabGeneral_screenshotsPathChanged (const QString &path);
	
	void shootDesktop ();
	void shootSelection ();
	void selectionMade (QRect selection);
	void shootWindow ();
	void uploadFile ();
	void uploadClipboard ();
	void uploadClipboardExt ();
	
	void disableRequested ();
	void preferencesRequested ();
	void quitRequested ();
	
private:
	PS_TabGeneral		*tabGeneral;
	PS_TabCredentials	*tabCredentials;
	PS_TabUpdates		*tabUpdates;
	
	PS_Tab			*currentTab;
	
	PS_SelectionWidget *selectionWidget;
	
	QEasingCurve	animCurve;
	QParallelAnimationGroup	*animation;
	
	QFileSystemWatcher *watcher;
	QNetworkAccessManager manager;

	QFile *inputFile;
	
	QGlobalShortcut scSelection, scWindow, scDesktop, scFile, scClipboard, scClipboardExt;
	QSystemTrayIcon *systemTrayIcon;
	
	/* System tray menu */
	QAction *actionMyAccount;
    QAction *actionSelectArea;
    QAction *actionDesktopScreenshot;
    QAction *actionWindowScreenshot;
    QAction *actionUploadFile;
	QAction *actionClipboard;
    QAction *actionDisablePuShot;
    QAction *actionPreferences;
    QAction *actionQuit;
    QMenu *menuTray;
	
	void setupTrayIcon ();
	
	QStringList lastDirectory;
	QString lastFile, lastFileHash;
	bool uploading, askExt, iconHeldDown;
	
	QStringList filesInDirectory () const;
	
	void uploadData (const QByteArray &data, const QString &fileName = "untitled");
	
	bool disabled;
	
signals:
	void quit ();
};

#endif /* defined (_PS_MAIN_WINDOW_H) */
