/********************************************************************************
 *																				*
 *		PS_NotificationMessage.mm - Created 16/12/2014 08:46					*
 *																				*
 *		By Tuetuopay - http://tuetuopay.fr										*
 *																				*
 *		This file is part of PuShot.											*
 *																				*
 *		PuShot is free software: you can redistribute it and/or modify			*
 *		it under the terms of the GNU General Public License as published by	*
 *		the Free Software Foundation, either version 3 of the License, or		*
 *		(at your option) any later version.										*
 *																				*
 *		PuShot is distributed in the hope that it will be useful,				*
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of			*
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
 *		GNU General Public License for more details.							*
 *																				*
 *		You should have received a copy of the GNU General Public License		*
 *		along with PuShot.  If not, see <http://www.gnu.org/licenses/>.			*
 *																				*
 ********************************************************************************/

#include "PS_NotificationMessage.h"
#include <QtCore>
#ifdef Q_OS_MAC
# include <Foundation/NSUserNotification.h>
#endif

PS_NotificationMessage::PS_NotificationMessage ()	{
	
}

PS_NotificationMessage::~PS_NotificationMessage ()	{
	
}

void PS_NotificationMessage::showMessage (const std::string &title, const std::string &message)	{
#ifdef Q_OS_MAC
	NSUserNotification *userNotification = [[[NSUserNotification alloc] init] autorelease];
	userNotification.title = [NSString stringWithCString:title.c_str () encoding:[NSString defaultCStringEncoding]];
	userNotification.informativeText = [NSString stringWithCString:message.c_str () encoding:[NSString defaultCStringEncoding]];
	
	//[[NSUserNotificationCenter defaultUserNotificationCenter] deliverNotification:userNotification];
	[[NSUserNotificationCenter defaultUserNotificationCenter] scheduleNotification:userNotification];
#endif
}
