/********************************************************************************
 *																				*
 *		PS_SelectionWidget.cpp - Created 01/09/2014 19:32						*
 *																				*
 *		By Tuetuopay - http://tuetuopay.fr										*
 *																				*
 *		This file is part of PuShot.											*
 *																				*
 *		PuShot is free software: you can redistribute it and/or modify			*
 *		it under the terms of the GNU General Public License as published by	*
 *		the Free Software Foundation, either version 3 of the License, or		*
 *		(at your option) any later version.										*
 *																				*
 *		PuShot is distributed in the hope that it will be useful,				*
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of			*
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
 *		GNU General Public License for more details.							*
 *																				*
 *		You should have received a copy of the GNU General Public License		*
 *		along with PuShot.  If not, see <http://www.gnu.org/licenses/>.			*
 *																				*
 ********************************************************************************/

#include "PS_SelectionWidget.h"

#include "PS_Utils.h"

PS_SelectionWidget::PS_SelectionWidget () : QWidget ()	{
	frame = new QFrame (this);
	frame->setGeometry (0, 0, 0, 0);
	
	QPalette pal (palette ());
	setWindowFlags (Qt::Tool | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
	pal.setColor (QPalette::Background, QColor (255, 255, 255, 16));
	setPalette (pal);
	setBackgroundRole (QPalette::NoRole);
	//setStyleSheet ("background: rgba(255, 255, 255, 0.5)");
	//setAttribute (Qt::WA_TranslucentBackground, true);
	
	pal = frame->palette ();
	frame->setFrameStyle (QFrame::Box | QFrame::Plain);
	frame->setLineWidth (1);
	// frame->setStyleSheet ("border: 1px solid white");
	frame->setAutoFillBackground (true);
	pal.setColor (QPalette::Foreground, QColor (0, 0, 0, 64));	/* Foreground is frame color. Because logic. */
	frame->setPalette (pal);
	
	selecting = false;

	hide ();
}

void PS_SelectionWidget::beginSelection ()	{
	frame->setGeometry (0, 0, 0, 0);
	setGeometry (PS_Utils::getDesktopGeometry ());
	show ();
}

void PS_SelectionWidget::mousePressEvent (QMouseEvent *event)	{
	if (event->button () == Qt::LeftButton)	{
		selecting = true;
		firstCorner = event->globalPos ();
		
		frame->setGeometry (firstCorner.x (), firstCorner.y (), 0, 0);
	}
}
void PS_SelectionWidget::mouseReleaseEvent (QMouseEvent *event)	{
	if (selecting)	{
		selecting = false;
		hide ();
		emit selectionFinished (PS_Utils::getRect (firstCorner, event->globalPos ()));
	}
}
void PS_SelectionWidget::keyPressEvent (QKeyEvent *event)	{
	if (event->key () == Qt::Key_Escape)	{
		selecting = false;
		hide ();
	}
}
void PS_SelectionWidget::mouseMoveEvent (QMouseEvent *event)	{
	if (selecting)	{
		frame->setGeometry (PS_Utils::getRect (event->globalPos (), firstCorner));
	}
}

PS_SelectionWidget::~PS_SelectionWidget ()	{
	
}