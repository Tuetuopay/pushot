/********************************************************************************
 *																				*
 *		PS_TabUpdates.cpp - Created 25/08/2014 01:20							*
 *																				*
 *		By Tuetuopay - http://tuetuopay.fr										*
 *																				*
 *		This file is part of PuShot.											*
 *																				*
 *		PuShot is free software: you can redistribute it and/or modify			*
 *		it under the terms of the GNU General Public License as published by	*
 *		the Free Software Foundation, either version 3 of the License, or		*
 *		(at your option) any later version.										*
 *																				*
 *		PuShot is distributed in the hope that it will be useful,				*
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of			*
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
 *		GNU General Public License for more details.							*
 *																				*
 *		You should have received a copy of the GNU General Public License		*
 *		along with PuShot.  If not, see <http://www.gnu.org/licenses/>.			*
 *																				*
 ********************************************************************************/

#include "PS_TabUpdates.h"

PS_TabUpdates::PS_TabUpdates (QWidget *parent) : PS_Tab (parent)	{
	setupUi (this);
	saveSize ();
}

void PS_TabUpdates::saveSettings (QSettings &s)	{
	
}
void PS_TabUpdates::loadSettings (QSettings &s)	{
	
}

PS_TabUpdates::~PS_TabUpdates () {}

